function Hora_segundos() {
  var transseg = document.getElementById("Convertidoseg");
  var Bminutos = 60;
  var Bhoras = 3600;

  var horaActual = new Date();
  var ho = horaActual.getHours();
  var mi = horaActual.getMinutes();
  var se = horaActual.getSeconds();
  var SegunHoras = ho * Bhoras;
  var segunMinutos = mi * Bminutos;
  var converR = parseInt(segunMinutos) + parseInt(SegunHoras) + parseInt(se);
  transseg.innerHTML =
    "Hora Actual: " +
    ho +
    ":" +
    mi +
    ":" +
    se +
    " Hora en Segundo Actual= " +
    converR;
}
function obtenerArea() {
  var areaResultado = document.getElementById("calculararea");
  var b = parseInt(document.getElementById("base").value);
  var h = parseInt(document.getElementById("altura").value);

  var area = (b * h) / 2;

  areaResultado.innerHTML = "La area del rectangulo es: " + area;
}
function RaizCuadrada() {
  var algo = document.getElementById("calcurarc");
  var mensage = "Solo damos resultados a valores impar que ingrese";
  var nuemero = document.getElementById("raizc").value;
  if (nuemero % 2) {
    var resultado = Math.sqrt(nuemero);
    algo.innerHTML = "" + resultado.toFixed(2);
  } else {
    algo.innerHTML = "" + mensage;
  }
}
function CadenaTexto() {
  var longitud = document.getElementById("cadenatotal");
  var entrada = document.getElementById("cadenaTexto").value;
  const newLocal = new String(entrada);
  var cadena = newLocal;
  longitud.innerHTML = "Su longitud es: " + cadena.length;
}
function ConcatenarArray() {
  var Resultadosemana = document.getElementById("Concatenar");
  var Dias_Laborables = new Array(
    "Lunes",
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes"
  );
  var Dias_Descanso = new Array("Sabado", "Domingo");
  var Semana = Dias_Laborables.concat(Dias_Descanso);
  Resultadosemana.innerHTML = "Dias de la Semana " + Semana;
}
function VersionNav() {
  document.write("Version del navegador: " + navigator.appVersion);
}
function AnchoAltura() {
  var mostrar = document.getElementById("mostrarresolucion");
  mostrar.innerHTML =
    "Resolucion de Pantalla es: " + screen.width + "x" + screen.height;
}
function ImprimirPages() {
  if (window.print) {
    window.print();
  }
}
